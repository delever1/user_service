package grpc

import (
	"delever/user_service/config"
	"delever/user_service/genproto/user_service"
	"delever/user_service/grpc/client"
	"delever/user_service/grpc/service"
	"delever/user_service/pkg/logger"
	"delever/user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterCourierServiceServer(grpcServer, service.NewCourierService(cfg, log, strg, srvc))
	user_service.RegisterClientServiceServer(grpcServer, service.NewClientService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
