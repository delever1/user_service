package service

import (
	"context"
	"delever/user_service/config"
	"delever/user_service/genproto/user_service"
	"delever/user_service/grpc/client"
	"delever/user_service/pkg/logger"
	"delever/user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ClientService) Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error) {
	u.log.Info("====== Client Create ======", logger.Any("req", req))

	resp, err := u.strg.Client().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ClientService) GetByUsername(ctx context.Context, req *user_service.ClientGetByUsernameReq) (*user_service.Client, error) {
	u.log.Info("====== Client GetByUsername ======", logger.Any("req", req))

	resp, err := u.strg.Client().GetByUsername(ctx, req)
	if err != nil {
		u.log.Error("error while getting client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
