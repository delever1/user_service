package service

import (
	"context"
	"delever/user_service/config"
	"delever/user_service/genproto/user_service"
	"delever/user_service/grpc/client"
	"delever/user_service/pkg/logger"
	"delever/user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *UserService {
	return &UserService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *UserService) Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error) {
	u.log.Info("====== User Create ======", logger.Any("req", req))

	resp, err := u.strg.User().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating user", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) GetByUsername(ctx context.Context, req *user_service.UserGetByUsernameReq) (*user_service.User, error) {
	u.log.Info("====== User GetByUsername ======", logger.Any("req", req))

	resp, err := u.strg.User().GetByUsername(ctx, req)
	if err != nil {
		u.log.Error("error while getting user", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) GetByEmail(ctx context.Context, req *user_service.UserGetByEmailReq) (*user_service.User, error) {
	u.log.Info("====== User GetByEmail ======", logger.Any("req", req))

	resp, err := u.strg.User().GetByEmail(ctx, req)
	if err != nil {
		u.log.Error("error while getting user", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) UpdatePassword(ctx context.Context, req *user_service.UserUpdatePasswordReq) (*user_service.UserUpdatePasswordResp, error) {
	u.log.Info("====== User UpdatePassword ======", logger.Any("req", req))

	resp, err := u.strg.User().UpdatePassword(ctx, req)
	if err != nil {
		u.log.Error("error while update password user", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
