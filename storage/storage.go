package storage

import (
	"context"
	"delever/user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	User() UserRepoI
	Courier() CourierRepoI
	Client() ClientRepoI
}

type BranchRepoI interface {
	Create(ctx context.Context, req *user_service.BranchCreateReq) (*user_service.BranchCreateResp, error)
	GetList(ctx context.Context, req *user_service.BranchGetListReq) (*user_service.BranchGetListResp, error)
	GetById(ctx context.Context, req *user_service.BranchIdReq) (*user_service.Branch, error)
	Update(ctx context.Context, req *user_service.BranchUpdateReq) (*user_service.BranchUpdateResp, error)
	Delete(ctx context.Context, req *user_service.BranchIdReq) (*user_service.BranchDeleteResp, error)
	GetBranchesActive(ctx context.Context, req *user_service.BranchesActiveGetReq) (*user_service.BranchGetListResp, error)
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error)
	GetByUsername(ctx context.Context, req *user_service.UserGetByUsernameReq) (*user_service.User, error)
	GetByEmail(ctx context.Context, req *user_service.UserGetByEmailReq) (*user_service.User, error)
	UpdatePassword(ctx context.Context, req *user_service.UserUpdatePasswordReq) (*user_service.UserUpdatePasswordResp, error)
}

type CourierRepoI interface {
	Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error)
	GetByUsername(ctx context.Context, req *user_service.CourierGetByUsernameReq) (*user_service.Courier, error)
	GetByEmail(ctx context.Context, req *user_service.CourierGetByEmailReq) (*user_service.Courier, error)
	UpdatePassword(ctx context.Context, req *user_service.CourierUpdatePasswordReq) (*user_service.CourierUpdatePasswordResp, error)
}

type ClientRepoI interface {
	Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error)
	GetByUsername(ctx context.Context, req *user_service.ClientGetByUsernameReq) (*user_service.Client, error)
}
