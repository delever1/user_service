CURRENT_DIR=$(shell pwd)

proto-gen:
	./scripts/gen-proto.sh  ${CURRENT_DIR}

pull-proto-module:
	git submodule update --init --recursive

update-proto-module:
	git submodule update --remote --merge

migrate-up:
	migrate -database postgres://postgres:password@localhost:5432/user_service\?sslmode\=disable -path migrations up

migrate-down:
	migrate -database postgres://postgres:password@localhost:5432/user_service\?sslmode\=disable -path migrations down
